package techtest.restsat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestSatelliteApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestSatelliteApplication.class, args);
	}

}
